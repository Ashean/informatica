const Sequelize = require('sequelize');

var sequelize = new Sequelize('sequelize', 'root', 'Abeysinghe02', {
    host: 'localhost',
    dialect: 'mariadb',
    pool: {
      max: 55, // Maximum number of connection in pool
      min: 0, // min ...
      idle: 10000 // The maximum time, in milliseconds, that a connection can be idle before being released
    },
});


// Or you can simply use a connection uri
// var sequelize = new Sequelize('mysql://sequelize:'sequelize@2019'@localhost:5432/sequelize');

var User = sequelize.define('user', {
    firstName: {
      type: Sequelize.STRING,
      field: 'first_name' // Will result in an attribute that is firstName when user facing but first_name in the database
    },
    lastName: {
      type: Sequelize.STRING,
      field: 'last_name'
    }
  }, {
    freezeTableName: true // Model tableName will be the same as the model name, not plural
  });
//METODO ASYNCRONO SENZA L' USO DEL CHAINIG DELLE RPOMISE
// Note: using `force: true` will drop the table if it already exists
/*User.sync({force: true}).then(function () {  //Il server tenta QUI la prima connessione
    // Table created
    return User.create({
      firstName: 'John',
      lastName: 'Hancock'
    });
  });

 /// Find all users, comment when
User.findAll().then(users => {
    console.log("All users:", JSON.stringify(users, null, 4));
  });
*/

//METODO 'SINCRONO' UTILIZZANDO IL METODO DI CONCATENAZIONE DELLE PROMISE
User.sync({force: true}).then(() => {
  return User.create({
    firstName: 'John',
    lastName: 'Hancock'
  });
}).then(() => {
  User.findAll().then(users => {
    console.log("All users:", JSON.stringify(users, null, 4));
  });
});

  // sequelize.close();
