const Sequelize = require('sequelize');

var sequelize = new Sequelize( 'sequelize','root', 'Abeysinghe02', {
    host: 'localhost',
    dialect: 'mariadb',
    pool: {
      max: 5,
      min: 0,
      idle: 10000
    },
});

var User = sequelize.define('user', {
    name: Sequelize.STRING,
    email: Sequelize.STRING
  }
);

var Project = sequelize.define('project', {
    name: Sequelize.STRING
  }
);

 //User.hasOne(Project); //ASSOCIAZIONE UNO AD UNO
 //User.hasMany(Project); //ASSOCIAZIONE UNO AD MOLTI
//Project.belongsTo(User); //ASSOCIAZIONE UNO AD UNO
Project.belongsToMany(User, {through: 'UserProject'});// ASSOCIAZINE MOLTI A MOLTI


sequelize.sync({force: true});
