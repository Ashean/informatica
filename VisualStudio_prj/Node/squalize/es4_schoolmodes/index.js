const Sequelize = require('sequelize');

var sequelize = new Sequelize('classicmodels', 'classicmodels', 'classicmodels', {
    host: 'localhost',
    dialect: 'mariadb',
    pool: {
      max: 5,
      min: 0,
      idle: 10000
    },
});

// import models

const Customer = sequelize.import(__dirname + '/models/customers'); // Pepe
const Employee = sequelize.import(__dirname + '/models/employees');
const Office = sequelize.import(__dirname + '/models/offices');
const OrderDetail = sequelize.import(__dirname + '/models/orderdetails');
const Order = sequelize.import(__dirname + '/models/orders');
const Payment = sequelize.import(__dirname + '/models/payments');
const ProductLine = sequelize.import(__dirname + '/models/productlines');
const Product = sequelize.import(__dirname + '/models/products');

Customer.hasMany(Order, {foreignKey: 'customerNumber'});
Order.belongsTo(Customer);