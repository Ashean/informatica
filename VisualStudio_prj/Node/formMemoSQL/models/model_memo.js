const Sequelize = require('sequelize');
const db = require('../config/db_manager');



const Memo = db.define('memo', {

    title: {
      type: Sequelize.STRING(30),
      field: 'title'
    },

    content :{
        type: Sequelize.STRING(1024),
        field: 'content'
    },

    level : {
        type: Sequelize.ENUM,
        values : ['1','2','3','4','5']
    }
  });


  module.exports = Memo;
