const express = require("express");
const route = express.Router();
const db = require("../config/db_manager");
const Memo = require("../models/model_memo");

route.get('/',(req,res) =>{
    res.render('main_template',{});
})

route.get('/load-all-memo',(req,res)=>{
    let memoList = {};
    Memo.findAll({}).then(result => {
        memoList = result;
        res.json({res: memoList});
    })
})

route.get('/load:id',(req,res)=>{
  let id = req.params.id;

  retrieveData(id).then(memo=>{
   res.json({res:memo});
  })

})


route.post('/newMemo',(req,res)=>{
    let title = req.body.title;
    let content = req.body.content;
    let level = req.body.level;

    Memo.create({
        title : title,
        content : content,
        level : level
    }).then(newMemo => {
        console.log('Salvato correttamente ' + newMemo);
        res.json({res : newMemo});
    })
});

route.post('/update-memo:id',(req,res)=>{
  let id = req.params.id;
  let title = req.body.title;
  let content = req.body.content;
  retrieveData(id).then(memo=>{
    memo.title = title;
    memo.content = content;
     updateElement(memo);
     res.json('Correttamente sovrascritto');
   })
});

route.delete('/delete-memo:id',(req,res)=>{
    let id = req.params.id;
    console.log(id)
  Memo.destroy({
    where: {
      id: id
    }
  }).then(
    console.log("Element successfully deleted"),
    res.json("Element successfully deleted")
  );

})

async function retrieveData(id){
  let memo = await  Memo.findOne({where : {id : id}});
  if(memo){
    return memo;
  }else{
    console.log('Nothing fuon, sorry ;(')
  }
};

async function updateElement(objModel){
  objModel.save();
};

module.exports = route;