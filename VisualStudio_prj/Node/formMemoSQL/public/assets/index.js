
let memoStyleLevel = [
  "notification is-info",
  "notification is-primary",
  "notification is-success",
  "notification is-warning",
  "notification is-danger",
];

//Total Stars
const starsTotal = 5;

$(document).ready(() => {
  pressRating;
  sendNewMemo;
  createFormMemo;
  createTagMemo;
  cancelForm;

  autosize($('textarea'));
  loadOldMemo();
});

/*__________IN MEMO FUNCTIONS________________________________________________________________________________________*/

function pressRating(tag,rate) {
  $(".box a").css("color", "#331133");

  for (let i = 0; i < rate; i++) {
    tag.querySelector(".b" + (i + 1)).css("color", "yellow");
  }
  level = rate;
  /*switch (rate) {
           case 1 :
               $('.box memo').css('color','green');            DA IMPLEMENTARE IL CAMBIO DI COLORE DEL FORM OGNI VOLTA CHE SI DA UNA RATE
               break;
       }*/
}

/*__________TOUCH DOM AND TAGS FUNCTIONS________________________________________________________________________________________*/

function createFormMemo() {
    $("#memoFormContainer").empty();
  let memo ='<div style="width:25%; margin:5%,7%,5%75%; float: left;" class="box memo"> <article class="media"><div class="media-content"><div class="content">   <p> <input class="input" type="text" placeholder="title"  required">   <br> <textarea class="textarea is-small" type="text" placeholder="content"  required></textarea> </p></div> <br><div class="field is-grouped"> <p class="control"><button class="button is-link" onclick="sendNewMemo(this)"> Save</button></p> <p class="control"><button class="button is-danger" onclick="cancelForm(this)"> Cancel</button></p></div></div></div></article></div>';
  $("#memoFormContainer").prepend(memo).show(400);
  return memo;
}

function cancelForm(tag) {
  $("#memoFormContainer").hide(400).empty();
}

function createTagMemo(data) {
  let levelRate = memoStyleLevel[data.level-1];
  let memoTag ='<div style="width: 25%;float: left;margin-left: 0.5%; margin-right: 0.5%;" class="box memo '+levelRate+'" onclick="letEditMemo(this)" id="'+data.id+'"><article class="media"><div class="media-content"><div class="content"><p><input class="input title is-5" type="text" value="'+data.title+'" style="border-width:0px;border:none;box-shadow: none;background: transparent;color: white" ><br><textarea class="textarea" type="text"  style="border-width:0px;border:none;box-shadow: none;background: transparent;color: white;  height: 50%;  "  >'+data.content+'</textarea></p></div></div><button class="delete is-danger" onclick="deleteCurrentMemo(this,'+data.id+')"></button></article><div  class="buttons has-addons is-right is-grouped" style="display : none;"><button class="button is-small is-link is-inverted" onclick="updateCurrentMemo(this,'+data.id+')" >Salva</button><button class="button is-small is-danger is-inverted" onclick="getOldCurrentMemo(this,'+data.id+')" >Annulla</button></div></div>';
  $("#allContainer").prepend(memoTag).show("200");
  $("#" + data.id + "article div div p textarea").value = data.content;
}

function letEditMemo(tag) {

  tag.lastChild.style = "display : true";


}

function cancelEditMemo(tag){
  tag.lastChild.style = "display : none";

}


/*__________REQUEST FUNCTIONS________________________________________________________________________________________*/

function sendNewMemo(tag) {
  let title = $("#memoFormContainer div article div div p input")
    .val()
    .toString();
  let content = $("#memoFormContainer div article div div p textarea")
    .val()
    .toString();
  let level = randomIdGenerator();
  let newMemo = {
    title: title,
    content: content,
    level: level,
  };
  $.post("/newMemo", newMemo, (data, status) => {
    let memo = data.res;
    console.log(data);
    createTagMemo(memo);
  });
}

function loadOldMemo() {
  $("#memoFormContainer").empty();
  $.get("/load-all-memo", (data, status) => {
    if (status == "success") {
      let memoList = data.res;
      for (let i = 0; i < memoList.length; i++) {
        createTagMemo(memoList[i]);
      }
    }
  });
  console.log("Memo vecchi caricati");
}

function getOldCurrentMemo(tag,id){
  let memoTag = $(tag).parent().parent()[0];
    $.get('/load' + id, (data,status)=>{
        let oldCurrentMemo = data.res;
        memoTag.querySelectorAll("input")[0].value= oldCurrentMemo.title;
        memoTag.querySelectorAll("textarea")[0].value = oldCurrentMemo.content;
        /*SISTEMAZIONE STELLINE*/
        cancelEditMemo( memoTag);
    })
}

function updateCurrentMemo(tag,id){
  let memoTag = $(tag).parent().parent()[0];
  let memoUp = {
    title :  memoTag.querySelectorAll("input")[0].value,
    content : memoTag.querySelectorAll("textarea")[0].value
  }
  $.post('update-memo'+id, memoUp, (data,status)=>{
      cancelEditMemo(memoTag);
    }
  )
}

function deleteCurrentMemo(tag,id){
  $.ajax({
      url: '/delete-memo' +id,
      type: 'DELETE',
      success: function(result) {
        $('#'+id).hide(350);
        $('#'+id).remove();


      }
  });
}

/*____________SUPPLEMENTERY FUNCTIONS ________________________________________________________________________*/

function randomIdGenerator() {
  var min = 1;
  var max = 6;
  var random = Math.floor(Math.random() * (+max - +min)) + +min;
  let n = random + "";
  return n;
}
