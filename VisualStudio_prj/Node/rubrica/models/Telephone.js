
const Sequelize = require('sequelize');
const db = require('../config/db_manager');

const Telephone = db.define('telephone',{


    telephone : {
        type : Sequelize.STRING(11),
        filed : 'telephone'
    },

    type : {
        type : Sequelize.ENUM,
        values : ['cellular','home','fax','tool-free','emergency']
    },

    id_user : {
        type: {type : Sequelize.INTEGER, unique : true, foreignKey: true},
        field: 'id_user' // Will result in an attribute that is firstName when user facing but first_name in the database
    }
});

module.exports = Telephone;