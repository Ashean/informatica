
const Sequelize = require('sequelize');
const db = require('../config/db_manager');



const Contact = db.define('contact', {
    
    name: {
      type: Sequelize.STRING(30),
      field: 'name'
    }
  });


  module.exports = Contact;