"use strict;"
var xhttp = new XMLHttpRequest();
$(window).on('load', function(){

    addTelephoneForm;

});


function addTelephoneForm(){
    var telephoneForm = '<div class="telephone" style="display:none; margin-top:2.5%"><div class="field"><label class="label">Type<a class="delete" style="margin-left :35%;" onclick="deleteCurrentTelephoneTag(this)"></a></label><div class="control"><div class="select"><select><option value="cellular" selected>Cellular</option><option value="home" >Home</option><option value="fax" >Fax</option><option value="tool-Free" >Tool-Free</option><option value="emergency" >Emergency</option></select></div></div></div><div class="field"><label class="label">Telephone number</label><div class="control"><input class="input" type="type" value="" style="width:20%;" required></div></div></div>';
    $('#telephoneContainer').append(telephoneForm);
    $('.telephone').last().show(300);
}

function deleteCurrentTelephoneTag(tag) {
    ($(tag)[0]).parentNode.parentNode.parentNode.hidden=true;


 }

function sendContact(){
    let telephone = [];
    var x = $('#telephoneContainer > div').length;

    for(let i =0; i< x; i++){

        let telCon = {
            type :$('#telephoneContainer div.telephone:eq('+i.toString()+') div div div select').val(),
            number : $('#telephoneContainer div.telephone:eq('+i.toString()+') div:odd div  input').val()
        }

        telephone.push(telCon);
    }
    let contact = {
        name : $('#contactName').val(),
        telephones : telephone
    }

    $.post('/addContact',contact, function(data,status){
        window.location.pathname="/";
    });
}