"use strict"
var xhr = new XMLHttpRequest();

$(window).on('load', function(){

  createContactTag;
  creatTelephoneTag;

$.get("/");


$.get('/load-all', (data,status )=>{
  let contacts = data.res;
  contacts.forEach(contact => {
    createContactTag(contact);
  });
})

});

/*___________________DOM MANAGAMENT METHODS______________________________*/

function createContactTag(contact){
  let objTag = ' <div class="contactButtons" id="'+contact.id+'" style="display:none;"><div class="contactContainers box media"><i class="far fa-user fa-2x" style="margin-right: 5%;"></i><strong class="media-content">'+ contact.name+' </strong> <a class="level-item" aria-label="reply" onclick="loadCurrentContact(('+ contact.id +'),this)"><span class="icon is-medium"><i class="fas fa-user-edit"  ></i></span></a><a class="delete is-medium" onclick="deleteCurrentContact(('+ contact.id +'),this)"></a> </div></div>';
  $('#addressContainer').append(objTag);
  $('#'+contact.id).show(400)
  return objTag;

};

function creatTelephoneTag(telephone,id){

let selectsOption=["","","","",""];
switch (telephone.type){
  case 'cellular' : selectsOption[0] = 'selected';
    break;
    case 'home' : selectsOption[1] = 'selected';
    break;
    case 'fax' : selectsOption[2] = 'selected';
    break;
    case 'tool-free' : selectsOption[3] = 'selected';
    break;
    case 'emergency' : selectsOption[4] = 'selected';
    break;
}
$('#'+telephone.id ).remove();
let currentTag= $('#'+id );
//let telephoneTag='<div class="box">cosa non va in te</div>';

let telephoneTag='<div id="'+telephone.id+'" class="media level" style="display:none; margin-bottom : 1%;padding-right: 5%; padding-left: 5%;  padding-bottom: 2%; border-top: none" ><input class="input" type="tel" value="'+telephone.telephone+'" style="margin-left:5%; margin-right: 5%; width: 20%"><div class="control"><div class="select"><select class="typeSelect" ><option value="cellular"  '+selectsOption[0]+'>Cellular</option><option value="home"  '+selectsOption[1]+'>Home</option><option value="fax"  '+selectsOption[2]+'>Fax</option><option value="tool-Free"  '+selectsOption[3]+'>Tool-Free</option><option value="emergency"  '+selectsOption[4]+'>Emergency</option></select></div></div> <div class="buttons"> <button class="button is-success level-right" onclick="updateTelephone('+telephone.id+',this)"><span class="icon is-small"><i class="fas fa-check"></i></span><span>Salva</span></button> <button class="button is-danger is-outlined" onclick="cancelUpdate('+telephone.id+')"><span>Annulla</span><span class="icon is-small"><i class="fas fa-times"></i></span></button></div></div>';
currentTag.append(telephoneTag);
$('#'+telephone.id).show(400);

//currentTag.eq(telephoneTag).append(telephoneTag);

return telephoneTag;

}

function cancelUpdate(id){
  $('#'+id).hide(300,()=>{$('#'+id).remove()});
}


function myFunction() {
  var input, filter, ul, li, a, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  ul = document.getElementById("addressContainer");
  li = ul.getElementsByClassName("contactButtons");
  for (i = 0; i < li.length; i++) {
      a = li[i].getElementsByTagName("strong")[0];
      console.log(a);
      txtValue = a.textContent || a.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
          li[i].style.display = "";
      } else {
          li[i].style.display = "none";
      }
  }
}

/*___________________REQUESTS METHODS ___________________________________*/

function updateTelephone(objId, tag){
  let tel = $('#'+objId).find('input').val();
  let ty = $('#'+objId).find('select').val();

  let telUp = {
      telephone : tel,
      type: ty
  }

  $.post("/update-telephone" + objId,telUp, (data,status) =>{
      let res = data;
      console.log(data);
      cancelUpdate(objId);
      }
  )
}


function addContact() {
  $.get('/addContact', (data,status)=>{
    if(status == 200){

    }else{
      console.log('Not able to load the form');
    }
  });
}

function loadCurrentContact(objId,tag){
  console.log($('#'+ objId));
   $.get('/load-current' + objId, function(data){
     let telephones = data.res.telephones;
     telephones.forEach(telephone => {
       creatTelephoneTag(telephone, objId);
     })
   });
}

function deleteCurrentContact(objId,tag){

  $.ajax({
        type: 'DELETE',
      url: '/' + objId,
      success: function (data) {
        $('#' + objId  ).hide(300);
      },
      error: function () {
        console.log("delete req doesn't work")
      }
    })

}