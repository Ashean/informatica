const express = require("express");
const route = express.Router();
const db = require("../config/db_manager");
const Contact = require("../models/Conctact");
const Telephone = require("../models/Telephone");

route.get("/",(req,res)=>{
  res.render('index');
})

route.get("/load-all", (req, res) => {
  Contact.findAll({
    order : [
      ['name','ASC']
    ]
  })
    .then(contacts => {
      res.json( { res: contacts });
    })
    .catch(err =>
      console.log("An error accourred during a GET REQUEST TO /" + err)
    );
});

route.get("/addContact", (req, res) => {
  res.render("form", { user: {} });
});

route.get("/load-current:id", (req, res) => {
  let id = req.params.id;
  console.log(id);
  let contactUp;
  let telephonesUp;
  let todo = {
    contact: {
      name: ""
    },
    telephones: []
  };
  Contact.findOne({
    where: { id: id }
  }).then(contact => {
    todo.contact.name = contact.name;
    // console.log(contactUp)
    Telephone.findAll({
      where: { id_user: contact.id }
    }).then(telephones => {
      telephones.forEach(function(telephone) {
        todo.telephones.push(telephone.dataValues);
      });
       console.log(todo);
      res.json( { res : todo })

    });
  });
});

route.post("/addContact", (req, res) => {
  let name = req.body.name;
  let telephones = req.body.telephones;

  Contact.create(
    {
      name: name
    },
    {
      include: [Telephone]
    }
  )
    .then(contact => {
      console.log("Successfully added to the DB ");
      res.json({ contacts: contact });
      telephones.forEach(telephone => {
        Telephone.create(
          {
            telephone: telephone.number,
            type: telephone.type,
            id_user: contact.id
          },
          {
            include: [Contact]
          }
        ).then(telephone => {
          console.log(
            "The element" + telephone + "is correctly saved in the db"
          );
        });
      });
    })
    .catch(err => {
      console.log(err);
    });
});

route.post("/update-telephone:id",(req,res)=>{
  let id = req.params.id;
  let telephone = req.body.telephone;
  let type = req.body.type;

  retrieveData(id,Telephone).then(el=>{
      el.telephone = telephone;
      el.type = type;
      updateElement(el);
      res.json('Correttamente sovrascritto');
  })
})

route.delete("/:id", (req, res) => {
  let id = req.params.id;
  Contact.destroy({
    where: {
      id: id
    }
  }).then(
    console.log("Element successfully deleted"),
    res.json("Element successfully deleted")
  );
});

async function retrieveData(id,Model){
  let obj = await  Model.findOne({where : {id : id}});
  if(obj){
    return obj;
  }else{
    console.log('Nothing fuon, sorry ;(')
  }
};

async function updateElement(objModel){
  objModel.save();
};



module.exports = route;
