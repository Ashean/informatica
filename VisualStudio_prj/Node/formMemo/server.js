"use strict";

const express = require("express");
const app = express();
const csv = require("csv-parser");
const fs = require("fs");
const bodyParser = require('body-parser')
const createCsvWriter = require("csv-writer").createObjectCsvWriter;


// set view engine
app.set("view engine", "ejs");
app.use(express.urlencoded());

app.use(
  bodyParser.urlencoded({
    extended: true
  })
)
app.use(bodyParser.json())
app.use("/assets", express.static("assets"));
app.use("/node_modules/bulma", express.static("bulma"));

/*app.locals.createNewMemo = function (newMemo){
  let x =
    "<div id=" +
    newMemo.id +
    ' "class="memo" style="display : none"><div class="box"><article class="media"><div class="media-content"> <div class="content"><p ><strong>' +
    newMemo.title +
    "</strong><br>" +
    newMemo.content +
    ' </p> </div> <div class="field is-grouped"> <p class="control"> <button class="button is-link" onclick="updateMemo()">Change</button> </p><p class="control"><button class="button is-danger" onclick="deleteMemo(' +
    newMemo.id +
    ')"> Delete </button> </p></div></div> </article></div> </div>';


  return x;
}
*/



console.log("The sserver has started");
let list = [];

app.get('/', function(req,res){
  console.log('Loading the first page');
  res.render('index',{});
})

app.get("/load-memo", function(req, res) {
  console.log("GET REQUEST received");
  let list = [];
  fs.createReadStream("./memoStore.csv")
    .pipe(csv())
    .on("data", CSVmemo => {
      let memo = {
        title: CSVmemo.title,
        content: CSVmemo.content,
        id: CSVmemo.id
      };
      list.push(memo);

    })
    .on("end", () => {
      console.log(list);
      //res.render('index',{todos: list});
      if(list[0].title != undefined){
      res.json({todos:list});
      }
    });


  });

app.post("/editMemo", function(req,res){
 //let editMemo = req.body.editMemo;
 console.log(req.body.id)
 list.find(memo => memo.id === req.body.id).title= "Memo1 (changed)";
 const csvWriter = createCsvWriter({
  path: "memoStore.csv",
  header: [
    { id: "title", title: "Title" },
    { id: "content", title: "Content" },
    { id: "id", title: "ID" }
  ]
});
csvWriter
  .writeRecords(list)
  .then(() => console.log("The CSV file was written successfully"));

 res.json("The memo with "+editMemo+"was updated");
})

app.post("/", function(req, res) {
  let name = req.body.title;
  let description = req.body.content;
  let id = req.body.id;
  const csvWriter = createCsvWriter({
    path: "memoStore.csv",
    header: [
      { id: "title", title: "title" },
      { id: "content", title: "content" },
      { id: "id", title: "id" }
    ]
  });
  if (name != NaN && description != NaN) {
    let memo = {
      title: name,
      content: description,
      id: id
    };
    list.push(memo);
    let arrayMemo = [];
    for (let i = 0; i < list.length; i++) {
      arrayMemo.push(list[i]);
    }

    csvWriter
      .writeRecords(arrayMemo)
      .then(() => console.log("The CSV file was written successfully"));
  }

  res.json({ message: "New memo added to the Db" });
});

app.delete("/:id", function(req, res) {
  list = list.filter(function(memo) {
    return memo.id.replace(/ /g, "-") !== req.params.id;
  });
  const csvWriter = createCsvWriter({
    path: "memoStore.csv",
    header: [
      { id: "title", title: "Title" },
      { id: "content", title: "Content" },
      { id: "id", title: "ID" }
    ]
  });
  csvWriter
    .writeRecords(list)
    .then(() => console.log("The CSV file was written successfully"));

  console.log("Cancellato memo con titolo " + req.params.id);
  res.json({ message: "Memo eliminato " });
});

app.listen(3000,() => console.log("Server ready"));
