"use strinct;";

$(document).ready(function() {
  $("#divForm").hide();
  $.get('/');
  loadOldMemo();
});

function showForm() {
  $("#divForm").show(300);
}

function hideForm() {
  $("#divForm").hide(300);
}

function deleteMemo(id) {
  let idToDel = "#" + id;
  let memoToEdit = $(idToDel);

  $(idToDel).fadeOut(250, function() {
    $(idToDel).remove();
    $.ajax({
      url: "/" + id,
      type: "DELETE",
      success: function(result) {
        console.log("Memo cancellato " + idToDel);
      }
    });
  });
}

 function loadOldMemo(){
   $.get('/load-memo',function(data,status){
     if(status == 'success'){
      console.log(data.todos);
      $('ul').empty();

       for(let i =0;i<data.todos.length ; i++){
        createNewMemo(data.todos[i]);
        console.log('Creato ' + data[i]);
       }
       console.log('GET REQUEST HAS SUCCESSED');



    }
   });
}

function postMemo() {
  let newMemo = {
    title: $("#memoTitle").val(),
    content: $("#memoContent").val(),
    id: randomIdGenerator()
  };
  $.post("/", newMemo, function(data, status) {
    console.log("New memo sended to the server");
    console.log(data);
    createNewMemo(newMemo);
  });
  $("#divForm").hide(200);
}

function updateMemo(id) {
  let idToUp = "#" + id;
  var $this = $( "#" + id);
  let editMemo = {
    title : $this.find('input').val(),
    content :  $this.find('textarea').text(),
    id : id
  }
  console.log("Put request on + " + editMemo.title);
  $.post("/editMemo",editMemo, function(data,status){
      console.log('The PUT REQUEST ended well')

  });
}

function createNewMemo(newMemo) {
  let x =
    "<div id=" +
    newMemo.id +
    ' class="memo" style="display : none"><article class="message is-info"><div class="media-content"> <div class="message-header"><input class="editInput" type="text" value="' +
    newMemo.title +
    '"><button class="delete" onclick="deleteMemo(' +
    newMemo.id +
    ')"> </button></div> <div class="message-body"><textarea class="editInput">'+
    newMemo.content +
    '</textarea> </div> </p><button onclick="updateMemo('+newMemo.id+')" class= "button is-link is-active"><i class="fas fa-edit"></i></button><p class="control"> </p></div> </<article> </div>';
  $("ul").prepend(x);
  $("#" + newMemo.id).fadeIn(300, function() {});

  return x;
}



function randomIdGenerator() {
  var min = 0;
  var max = 1000000000000;
  var random = Math.floor(Math.random() * (+max - +min)) + +min;
  let n = random + "";
  return n;
}
