-- MySQL dump 10.16  Distrib 10.1.41-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: School
-- ------------------------------------------------------
-- Server version	10.1.41-MariaDB-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Grades`
--

DROP TABLE IF EXISTS `Grades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Grades` (
  `id_student` int(3) NOT NULL,
  `grade` int(3) NOT NULL,
  `id_subject` int(3) NOT NULL,
  KEY `id_student` (`id_student`),
  KEY `id_subject` (`id_subject`),
  CONSTRAINT `Grades_ibfk_1` FOREIGN KEY (`id_student`) REFERENCES `Students` (`id`),
  CONSTRAINT `Grades_ibfk_2` FOREIGN KEY (`id_subject`) REFERENCES `Subject` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Grades`
--

LOCK TABLES `Grades` WRITE;
/*!40000 ALTER TABLE `Grades` DISABLE KEYS */;
INSERT INTO `Grades` VALUES (1,6,1),(2,4,1),(3,7,1),(4,8,1),(5,10,1),(6,4,1),(7,6,1),(8,5,1),(9,8,1),(10,7,1),(11,6,1),(12,6,1),(13,5,1),(14,6,1),(15,6,1),(16,7,1),(17,5,1),(18,6,1),(19,4,1),(20,6,1),(21,6,1),(22,7,1),(23,6,1),(1,6,2),(2,6,2),(3,6,2),(4,6,2),(5,4,2),(6,6,2),(7,6,2),(8,8,2),(9,8,2),(10,8,2),(11,6,2),(12,4,2),(13,6,2),(14,6,2),(15,6,2),(16,5,2),(17,6,2),(18,4,2),(19,6,2),(20,6,2),(21,4,2),(22,3,2),(23,2,2),(1,6,3),(2,6,3),(3,7,3),(4,5,3),(5,6,3),(6,4,3),(7,6,3),(8,6,3),(9,7,3),(10,6,3),(11,6,3),(12,6,3),(13,6,3),(14,6,3),(15,4,3),(16,4,3),(17,4,3),(18,5,3),(19,7,3),(20,7,3),(21,7,3),(22,8,3),(23,6,3),(1,6,4),(2,6,4),(3,4,4),(4,4,4),(5,4,4),(6,5,4),(7,10,4),(8,10,4),(9,9,4),(10,8,4),(11,8,4),(12,8,4),(13,5,4),(14,6,4),(15,6,4),(16,4,4),(17,4,4),(18,4,4),(19,5,4),(20,7,4),(21,6,4),(22,6,4),(23,6,4),(1,4,5),(2,5,5),(3,7,5),(4,7,5),(5,7,5),(6,8,5),(7,6,5),(8,6,5),(9,6,5),(10,4,5),(11,4,5),(12,4,5),(13,5,5),(14,10,5),(15,10,5),(16,9,5),(17,8,5),(18,8,5),(19,3,5),(20,4,5),(21,5,5),(22,6,5),(23,7,5),(1,6,1),(2,7,1),(3,8,1),(4,5,1),(5,10,1),(6,10,1),(7,9,1),(8,8,1),(9,8,1),(10,8,1),(11,5,1),(12,6,1),(13,6,1),(14,4,1),(15,4,1),(16,4,1),(17,5,1),(18,7,1),(19,6,1),(20,6,1),(21,6,1),(22,4,1),(23,6,1),(1,6,2),(2,6,2),(3,5,2),(4,6,2),(5,5,2),(6,6,2),(7,6,2),(8,4,2),(9,4,2),(10,4,2),(11,5,2),(12,7,2),(13,6,2),(14,6,2),(15,6,2),(16,4,2),(17,6,2),(18,6,2),(19,6,2),(20,6,2),(21,6,2),(22,6,2),(23,6,2),(1,5,3),(2,5,3),(3,6,3),(4,6,3),(5,4,3),(6,4,3),(7,4,3),(8,5,3),(9,7,3),(10,6,3),(11,6,3),(12,6,3),(13,4,3),(14,6,3),(15,6,3),(16,6,3),(17,6,3),(18,6,3),(19,6,3),(20,7,3),(21,6,3),(22,5,3),(23,6,3),(1,6,4),(2,4,4),(3,4,4),(4,4,4),(5,5,4),(6,7,4),(7,6,4),(8,6,4),(9,6,4),(10,4,4),(11,5,4),(12,5,4),(13,6,4),(14,6,4),(15,4,4),(16,4,4),(17,4,4),(18,5,4),(19,7,4),(20,6,4),(21,6,4),(22,6,4),(23,4,4),(1,6,5),(2,6,5),(3,6,5),(4,6,5),(5,6,5),(6,10,5),(7,7,5),(8,5,5),(9,6,5),(10,6,5),(11,4,5),(12,4,5),(13,4,5),(14,5,5),(15,7,5),(16,6,5),(17,6,5),(18,6,5),(19,4,5),(20,6,5),(21,6,5),(22,5,5),(23,6,5),(1,6,1),(2,6,1),(3,10,1),(4,7,1),(5,5,1),(6,6,1),(7,6,1),(8,4,1),(9,8,1),(10,8,1),(11,6,1),(12,6,1),(13,6,1),(14,4,1),(15,4,1),(16,4,1),(17,7,1),(18,7,1),(19,7,1),(20,7,1),(21,6,1),(22,6,1),(23,10,1),(1,7,2),(2,5,2),(3,6,2),(4,6,2),(5,4,2),(6,6,2),(7,6,2),(8,4,2),(9,4,2),(10,4,2),(11,5,2),(12,5,2),(13,6,2),(14,7,2),(15,7,2),(16,9,2),(17,10,2),(18,6,2),(19,6,2),(20,6,2),(21,4,2),(22,6,2),(23,7,2),(1,6,3),(2,6,3),(3,6,3),(4,6,3),(5,6,3),(6,4,3),(7,8,3),(8,5,3),(9,7,3),(10,6,3),(11,5,3),(12,6,3),(13,4,3),(14,6,3),(15,6,3),(16,6,3),(17,6,3),(18,6,3),(19,6,3),(20,7,3),(21,6,3),(22,5,3),(23,6,3),(1,6,4),(2,6,4),(3,6,4),(4,6,4),(5,5,4),(6,7,4),(7,6,4),(8,6,4),(9,6,4),(10,4,4),(11,5,4),(12,5,4),(13,6,4),(14,6,4),(15,4,4),(16,4,4),(17,4,4),(18,5,4),(19,7,4),(20,6,4),(21,3,4),(22,3,4),(23,4,4),(1,6,5),(2,6,5),(3,6,5),(4,3,5),(5,3,5),(6,10,5),(7,7,5),(8,5,5),(9,6,5),(10,6,5),(11,4,5),(12,4,5),(13,4,5),(14,5,5),(15,7,5),(16,6,5),(17,3,5),(18,3,5),(19,4,5),(20,6,5),(21,6,5),(22,5,5),(23,6,5);
/*!40000 ALTER TABLE `Grades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Students`
--

DROP TABLE IF EXISTS `Students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Students` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `surname` varchar(30) NOT NULL,
  `sex` enum('M','F') DEFAULT NULL,
  `birth_year` year(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Students`
--

LOCK TABLES `Students` WRITE;
/*!40000 ALTER TABLE `Students` DISABLE KEYS */;
INSERT INTO `Students` VALUES (1,'Piero','Marin','M',2001),(2,'Andrea','Susa','M',2001),(3,'Giovanni','Amodeo','M',2001),(4,'Rossella','Rossi','F',1998),(5,'Paolo','Rossi','M',2002),(6,'Maria','Viva','F',2000),(7,'Marco','Pilastro','M',2001),(8,'Anna','Tavola','F',2001),(9,'Marco','Firma','M',2001),(10,'Angela','Roma','F',2001),(11,'Alessio','Furbo','M',2001),(12,'Pino','Tagliabirra','M',2001),(13,'Vanni','Nono','M',1999),(14,'Angelo','Careano','M',2001),(15,'Thomas','Guji','M',2001),(16,'Rino','Mattei','M',2001),(17,'Luca','Gava','M',2001),(18,'Josha','Tree','M',2001),(19,'Yin','Fin','F',2002),(20,'Guglielmo','Tello','M',2001),(21,'Anna','Testi','F',2001),(22,'Wilma','Winger','F',2001),(23,'Roberto','Casarin','M',2001);
/*!40000 ALTER TABLE `Students` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Subject`
--

DROP TABLE IF EXISTS `Subject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Subject` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Subject`
--

LOCK TABLES `Subject` WRITE;
/*!40000 ALTER TABLE `Subject` DISABLE KEYS */;
INSERT INTO `Subject` VALUES (1,'Informatica'),(2,'Italiano'),(3,'Matematica'),(4,'Sistemi'),(5,'Storia');
/*!40000 ALTER TABLE `Subject` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-02-18 12:50:57
