# School

- dare i permessi `ALL` sul database all'utente `school` con  password `school@2020`


- importare le tabelle dai file csv
- aggiungere i **vincoli**: chiavi primarie, chiavi esterne, `NOT NULL` ed integrità referenziale con clausole (`ON UPDATE` e `ON DELETE`)
- aggiungere due valutazioni a studenti esistenti

## query 

1. Si trovi la media dei voti per materia su due colonne: `average` e `sobject`.
  
   SELECT AVG(Grades.grade) AS average, Subject.name AS subject  from Grades JOIN Subject ON (Grades.id_subject = Subject.id) GROUP BY Grades.id_subject;


2. Si trovino gli studenti che in Matematica hanno almeno un voto superiore alla media dei voti in Matematica di tutti gli studenti.

    SELECT Students.name, Students.surname, MAX(Grades.grade) AS VotoMax
    FROM Grades
    JOIN (Students, Subject) ON Grades.id_subject = Subject.id AND Grades.id_student = Students.id
     WHERE Subject.name = "Matematica"
     GROUP BY Students.name , Students.surname, Subject.name 
     HAVING VotoMax >= (SELECT AVG(Grades.grade) AS average  from Grades JOIN Subject ON (Grades.id_subject = Subject.id)  WHERE Subject.name = "Matematica" GROUP BY Grades.id_subject);
)

3. Si trovino gli studenti nati nel `2001` che hanno ricevuto almeno una valutazione insufficiente in Informatica.

    SELECT Students.name, Students.surname, Students.birth_year, MIN(Grades.grade) AS Voto
    FROM Students
    JOIN (Grades, Subject) ON Grades.id_subject = Subject.id AND Grades.id_student = Students.id
    WHERE (Students.birth_year = "2001" AND Subject.name ="Informatica")
    GROUP BY Students.name
    HAVING Voto < 6; 


4. Quali sono gli studenti di sesso femminile ad avere la media migliore (si tenga comto del fatto che il voto per materia è dato dalla media dei voti).

     SELECT Students.name, Students.surname, Students.birth_year, MAX(Grades.grade) AS Voto
    FROM Students
    JOIN (Grades, Subject) ON Grades.id_subject = Subject.id AND Grades.id_student = Students.id
    WHERE (Students.sex = 'F')
    GROUP BY Students.name , Students.surname, Subject.name 
     HAVING Voto >= (SELECT AVG(Grades.grade) AS average  from Grades JOIN Subject ON (Grades.id_subject = Subject.id);
)