**File conente le query del db ditta **

Decisioni conseguite sullo sviluppo del file csv :
Ci saranno in totale 4 uffici;
I capi assoluti faranno parte del prorpio ufficio (0) 
Tutti i restanti 
*  Capi Assoluti * I capi assoluti della azienda (con idCapo = null) sono Lucia Ragazzo e Flaviu Enache
*  Dipendenti (non capi)*
1.  Federico Borme
2.  Lorenzo Malavolta 
3.  Anna Stefanello
4.  Beatrice Bolognato
5.  Alice Cesco
6.  Chiara Durante
7.  Davide Montesi 
8.  Gianluca Condotta

*Dipendenti capo*
1.  Greta Crivellaro (1)
2.  Miriam Mizgur (2)
3.  Riccardo Lipari (3)
4.  Simone Gasparini (4)


Comandi usati 

Creazione del DB:
*  CREATE DATABASE ditta;
*  CREATE USER 'ditta' IDENTIFIED BY 'ditta@2020';
*  GRANT USAGE ON *.* TO 'ditta'@'%' IDENTIFIED BY 'ditta@2020';
* CREATE TABLE dipendenti (ssn VARCHAR(11), nome VARCHAR(40), ufficio INT, idCapo VARCHAR(11)); 
*    LOAD DATA LOCAL INFILE 'C:/Users/ashea/Documents/I.T.I.S. Zuccante/_Triennio/5IB/informatica/testSql/Ditta/Dipendenti.csv' IGNORE INTO TABLE dipendenti FIELDS TERMINATED BY ‘,’ LINES TERMINATED BY ‘\n’;
*     ALTER TABLE dipendenti ADD PRIMARY KEY (ssn);
*     ALTER TABLE dipendenti ADD UNIQUE KEY (ssn);

Query:
*   SELECT dipendenti.nome, capi.nome FROM dipendenti  JOIN dipendenti AS capi ON dipendenti.idCapo= capi.ssn;
+--------------------+------------------+
| nome               | nome             |
+--------------------+------------------+
| Davide Montesi     | Riccardo Lipari  |
| Alice Cesco        | Greta Crivellaro |
| Riccardo Lipari    | Lucia Ragazzo    |
| Lorenzo Malavolta  | Miriam Mizgur    |
| Simone Gasparini   | Lucia Ragazzo    |
| Federico Borme     | Greta Crivellaro |
| Gianluca Condotta  | Simone Gasparini |
| Beatrice Bolognato | Simone Gasparini |
| Miriam Mizgur      | Flaviu Enache    |
| Chiara Durante     | Miriam Mizgur    |
| Greta Crivellaro   | Flaviu Enache    |
| Anna Stefanello    | Riccardo Lipari  |
+--------------------+------------------+

*   SELECT dipendenti.ufficio, capi.nome FROM dipendenti  JOIN dipendenti AS capi ON dipendenti.idCapo= capi.ssn GROUP BY dipendenti.ufficio;
+---------+------------------+
| ufficio | nome             |
+---------+------------------+
|       1 | Greta Crivellaro |
|       2 | Miriam Mizgur    |
|       3 | Riccardo Lipari  |
|       4 | Lucia Ragazzo    |
+---------+------------------+

*   SELECT dipendenti.nome, dipendenti.ufficio FROM dipendenti ;
    +--------------------+---------+
| nome               | ufficio |
+--------------------+---------+
| Davide Montesi     |       3 |
| Lucia Ragazzo      |       0 |
| Alice Cesco        |       1 |
| Riccardo Lipari    |       3 |
| Lorenzo Malavolta  |       2 |
| Simone Gasparini   |       4 |
| Federico Borme     |       1 |
| Gianluca Condotta  |       4 |
| Beatrice Bolognato |       4 |
| Miriam Mizgur      |       2 |
| Flaviu Enache      |       0 |
| Chiara Durante     |       2 |
| Greta Crivellaro   |       1 |
| Anna Stefanello    |       3 |
+--------------------+---------+

*   


