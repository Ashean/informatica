**File contenente tutte le query del db Museo**


Prima di cominciare con le vere query, abbiamo effetuttuato delle query di "pulizia" delle colonnne che non ci servivano: 

- ALTER TABLE tblArtisti DROP nazione;
- ALTER TABLE tblArtisti DROP correnteArtistica;
- ALTER TABLE tblArtisti DROP nomeCognome;

- ALTER TABLE tblOpere DROP sala;
- ALTER TABLE tblOpere DROP autore;


Query esercizio;

A) SELECT nazione,nome,cognome FROM tblArtisti JOIN (tblNazione) ON (tblArtisti.idNazione = tblNazione.idNazione) ORDER BY cognome;

B) SELECT titolo,annoFine  FROM tblArtisti JOIN (tblOpere) ON (tblArtisti.idArtista = tblOpere.idArtista) WHERE (nome = "Tiziano" AND cognome = "Vecellio") ORDER BY titolo ;


C) SELECT sala FROM tblArtisti JOIN (tblOpere,tblSale)
	ON (tblArtisti.idArtista = tblOpere.idArtista
	AND tblOpere.idSala= tblSale.idSala) WHERE (nome = "Vincent" AND cognome = "Van Gogh");

D) SELECT sala  FROM tblArtisti JOIN (tblOpere,tblSale,tblCorrenteArtistica) 
	ON (tblArtisti.idArtista = tblOpere.idArtista AND tblOpere.idSala= tblSale.idSala AND tblArtisti.idCorrenteArtistica=tblCorrenteArtistica.idCorrenteArtistica) 
	WHERE (correnteArtistica = "Impressionismo") GROUP BY tblSale.sala 

E) SELECT nome,cognome,dataNascita,dataMorte,nazione  FROM tblArtisti JOIN 	(tblSale,tblNazione,tblOpere) 
	ON (tblArtisti.idNazione = tblNazione.idNazione AND tblOpere.idSala= tblSale.idSala AND tblArtisti.idArtista=tblOpere.idArtista) 
	WHERE (sala = "Italia1") ORDER BY cognome,nome;

F) SELECT titolo,annoFine,tecnica,dimensioni  FROM tblArtisti JOIN (tblCorrenteArtistica,tblOpere)
	 ON 		( tblArtisti.idArtista=tblOpere.idArtista AND tblCorrenteArtistica.idCorrenteArtistica=tblArtisti.idCorrenteArtistica) 
	WHERE (correnteArtistica = "Rinascimento") ORDER BY titolo;

G) SELECT sala  FROM tblArtisti JOIN (tblOpere,tblSale,tblCorrenteArtistica) 
	ON (tblArtisti.idArtista = tblOpere.idArtista AND tblOpere.idSala= tblSale.idSala AND tblArtisti.idCorrenteArtistica=tblCorrenteArtistica.idCorrenteArtistica)
	 WHERE (correnteArtistica = "Impressionismo") GROUP BY sala ORDER BY sala ;






