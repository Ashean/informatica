DROP TABLE IF EXISTS `tblArtisti`;

CREATE TABLE `tblArtisti` (
  `idArtista` INT NOT NULL PRIMARY KEY,
  `cognome` VARCHAR(12) NOT NULL,
  `nome` VARCHAR(14) NOT NULL,
  `dataNascita` VARCHAR(10) NOT NULL,
  `dataMorte` VARCHAR(10) NOT NULL,
  `nazione` VARCHAR(11) NOT NULL,
  `correnteArtistica` VARCHAR(14) NOT NULL,
  `nomeCognome` VARCHAR(23) NOT NULL,
  `idCorrenteArtistica` INT NOT NULL,
  `idNazione` INT NOT NULL
) ENGINE=InnoDB CHARACTER SET=latin1;

INSERT INTO `tblArtisti` VALUES (1,'Fattori','Giovanni','06/09/1825','30/08/1908','Italia','Macchiaioli','Giovanni Fattori',1,1),(2,'De Tivoli','Serafino','22/02/1825','01/11/1892','Italia','Macchiaioli','Serafino De Tivoli',1,1),(3,'Monet','Claude','14/11/1840','05/12/1926','Francia','Impressionismo','Claude Monet',2,2),(4,'Renoir','Pierre-Auguste','25/02/1841','03/12/1919','Francia','Impressionismo','Pierre-Auguste Renoir',2,2),(5,'Degas','Edgar','19/07/1834','27/09/1917','Francia','Impressionismo','Edgar Degas',2,2),(6,'Sisley','Alfred','30/10/1839','29/01/1899','Inghilterra','Impressionismo','Alfred Sisley',2,3),(7,'Zandomeneghi','Federico','02/06/1841','31/12/1917','Italia','Impressionismo','Federico Zandomeneghi',2,1),(8,'Vecellio','Tiziano','1488 ?','27/08/1576','Italia','Rinascimento','Tiziano Vecellio',3,1),(9,'Buonarroti','Michelangelo','06/03/1475','18/02/1564','Italia','Rinascimento','Michelangelo Buonarroti',3,1),(10,'Dalì','Salvador','11/05/1904','23/01/1989','Spagna','Surrealismo','Salvador Dalì',4,4),(11,'Picasso','Pablo','25/10/1881','08/04/1973','Spagna','Cubismo','Pablo Picasso',5,4),(12,'Canova','Antonio','01/11/1757','13/10/1822','Italia','Neoclassicismo','Antonio Canova',6,1),(13,'Vermeer','Jan','1632','15/12/1675','Olanda','Barocco','Jan Vermeer',7,5),(14,'van Gogh','Vincent','30/03/1853','29/07/1890','Olanda','Impressionismo','Vincent van Gogh',2,5);

DROP TABLE IF EXISTS `tblCorrenteArtistica`;

CREATE TABLE `tblCorrenteArtistica` (
  `idCorrenteArtistica` INT NOT NULL PRIMARY KEY,
  `correnteArtistica` VARCHAR(14) NOT NULL
) ENGINE=InnoDB CHARACTER SET=latin1;

INSERT INTO `tblCorrenteArtistica` VALUES (1,'Macchiaioli'),(2,'Impressionismo'),(3,'Rinascimento'),(4,'Surrealismo'),(5,'Cubismo'),(6,'Neoclassicismo'),(7,'Barocco');

DROP TABLE IF EXISTS `tblNazione`;

CREATE TABLE `tblNazione` (
  `idNazione` INT NOT NULL PRIMARY KEY,
  `nazione` VARCHAR(11) DEFAULT NULL
) ENGINE=InnoDB CHARACTER SET=latin1;

INSERT INTO `tblNazione` VALUES (1,'Italia'),(2,'Francia'),(3,'Inghilterra'),(4,'Spagna'),(5,'Olanda');

DROP TABLE IF EXISTS `tblOpere`;

CREATE TABLE `tblOpere` (
  `idOpera` INT NOT NULL PRIMARY KEY,
  `titolo` VARCHAR(51) NOT NULL,
  `annoFine` VARCHAR(6) NOT NULL,
  `tipo` VARCHAR(8) NOT NULL,
  `tecnica` VARCHAR(17) NOT NULL,
  `dimensioni` VARCHAR(21) NOT NULL,
  `autore` VARCHAR(23) NOT NULL,
  `sala` VARCHAR(12) NOT NULL,
  `idArtista` INT NOT NULL,
  `idSala` INT NOT NULL
) ENGINE=InnoDB CHARACTER SET=latin1;

INSERT INTO `tblOpere` VALUES (1,'Gli ombrelli',1886,'Pittura','Olio su tela','180×115 cm','Pierre-Auguste Renoir','Francia1',4,1),(2,'Ballerina di quattordici anni',1881,'Scultura','Cera','98 cm','Edgar Degas','Francia1',5,1),(3,'L\'assenzio',1876,'Pittura','Olio su tela','68 x 92 cm','Edgar Degas','Francia1',5,1),(4,'La lezione di danza',1875,'Pittura','Olio su tela','85×75 cm','Edgar Degas','Francia2',5,2),(5,'Ballerine',1885,'Pittura','Pastello','75×73 cm','Edgar Degas','Francia2',5,2),(6,'Le bagnanti',1919,'Pittura','Olio su tela','60×110 cm','Pierre-Auguste Renoir','Francia2',4,2),(7,'La colazione dei canottieri',1882,'Pittura','Olio su tela','129,5×172,5 cm','Pierre-Auguste Renoir','Francia3',4,3),(8,'Colazione sull\'erba',1866,'Pittura','Olio su tela','248×217 cm','Claude Monet','Francia3',3,3),(9,'La terrazza a Sainte-Adresse',1867,'Pittura','Olio su tela','98,1×129,9 cm','Claude Monet','Francia3',3,3),(10,'I papaveri',1873,'Pittura','Olio su tela','50×65 cm','Claude Monet','Francia3',3,3),(11,'Le barche, regate ad Argenteuil',1874,'Pittura','Olio su tela','60×100 cm','Claude Monet','Francia3',3,3),(12,'La passeggiata',1875,'Pittura','Olio su tela','100×81 cm','Claude Monet','Francia3',3,3),(13,'Covone a Giverny',1886,'Pittura','Olio su tela','60,5×81,5 cm','Claude Monet','Francia3',3,3),(14,'Saggio di figura en plein air',1886,'Pittura','Olio su tela','131×88 cm','Claude Monet','Francia3',3,3),(15,'Sentiero a Louveciennes',1973,'Pittura','Olio su tela','38×46,5 cm','Alfred Sisley','Inghilterra1',6,4),(16,'La nebbia, Voisins',1874,'Pittura','Olio su tela','50,5×65 cm','Alfred Sisley','Inghilterra1',6,4),(17,'Place d\'Anvers, Paris',1880,'Pittura','Olio su tela','100x135 cm','Federico Zandomeneghi','Italia1',7,5),(18,'Palazzo Pretorio',1865,'Pittura','Olio su tela','81x63 cm','Federico Zandomeneghi','Italia1',7,5),(19,'Al pascolo',1859,'Pittura','Olio su tela','102x73 cm','Serafino De Tivoli','Italia1',2,5),(20,'Autoritratto',1854,'Pittura','Olio su tela','59×47 cm','Giovanni Fattori','Italia1',1,5),(21,'David',1504,'Scultura','Marmo','517×199 cm','Michelangelo Buonarroti','Italia1',9,5),(22,'La pietà',1499,'Scultura','Marmo','174×195×69 cm','Michelangelo Buonarroti','Italia2',9,6),(23,'Bacco',1497,'Scultura','Marmo','203 cm','Michelangelo Buonarroti','Italia2',9,6),(24,'Tondo Doni',1504,'Pittura','Tempera su tavola','120×120 cm','Michelangelo Buonarroti','Italia2',9,6),(25,'Peccato originale e cacciata dal Paradiso terrestre',1510,'Pittura','Affresco','280×570 cm','Michelangelo Buonarroti','Italia2',9,6),(26,'Creazione di Adamo',1511,'Pittura','Affresco','280×570 cm','Michelangelo Buonarroti','Italia2',9,6),(27,'Diluvio universale',1508,'Pittura','Affresco','280×560 cm','Michelangelo Buonarroti','Italia2',9,6),(28,'Amor sacro e Amor profano',1515,'Pittura','Olio su tela','118×278 cm','Tiziano Vecellio','Italia1',8,5),(29,'Tre età dell\'uomo',1512,'Pittura','Olio su tela','106×182 cm','Tiziano Vecellio','Italia3',8,7),(30,'Assunta',1518,'Pittura','Olio su tavola','690×360 cm','Tiziano Vecellio','Italia3',8,7),(31,'Bacco e Arianna',1523,'Pittura','Olio su tela','176,5×191 cm','Tiziano Vecellio','Italia3',8,7),(32,'Ritratto di Carlo V a cavallo',1548,'Pittura','Olio su tela','332×279 cm','Tiziano Vecellio','Italia3',8,7),(33,'Ritratto di Francesco Maria Della Rovere',1538,'Pittura','Olio su tela','114×103 cm','Tiziano Vecellio','Italia3',8,7),(34,'La persistenza della memoria',1931,'Pittura','Olio su tela','24×33 cm','Salvador Dalì','Spagna1',10,12),(35,'Il grande masturbatore',1929,'Pittura','Olio su tela','110×150 cm','Salvador Dalì','Spagna1',10,12),(36,'Giraffa in fiamme',1937,'Pittura','Olio su tela','35×27 cm','Salvador Dalì','Spagna1',10,12),(37,'Ragazza alla finestra',1925,'Pittura','Olio su tela','103×75 cm','Salvador Dalì','Spagna1',10,12),(38,'Tentazioni di sant\'Antonio',1946,'Pittura','Olio su tela','90×120 cm','Salvador Dalì','Spagna1',10,12),(39,'Madonna di Port Lligat',1969,'Scultura','Bronzo','41 cm','Salvador Dalì','Spagna1',10,12),(40,'Les demoiselles d\'Avignon',1907,'Pittura','Olio su tela','243,9×233,7 cm','Pablo Picasso','Spagna1',11,12),(41,'Guernica',1937,'Pittura','Olio su tela','349,3×776,6 cm','Pablo Picasso','Spagna1',11,12),(42,'Arlecchino pensoso',1901,'Pittura','Olio su tela','82,7×61,2 cm','Pablo Picasso','Spagna2',11,13),(43,'Il vecchio chitarrista cieco',1903,'Pittura','Olio su tela','121×92 cm','Pablo Picasso','Spagna2',11,13),(44,'Ritratto di Marie-Thérèse',1937,'Pittura','Olio su tela','100×81 cm','Pablo Picasso','Spagna2',11,13),(45,'Due donne che corrono sulla spiaggia',1922,'Pittura','Olio su tela','32,5×41,1 cm','Pablo Picasso','Spagna2',11,13),(46,'Ritratto di Dora Maar',1937,'Pittura','Olio su tela','92×65 cm','Pablo Picasso','Spagna2',11,13),(47,'Donne di Algeri',1955,'Pittura','Olio su tela','114×146 cm','Pablo Picasso','Spagna2',11,13),(48,'Le Fou (Il Matto)',1905,'Scultura','Bronzo','41 cm','Pablo Picasso','Spagna2',11,13),(49,'Testa femminile (Fernande)',1906,'Scultura','Bronzo','40.5 x 23 x 26 cm','Pablo Picasso','Spagna2',11,13),(50,'Mandolino e clarinetto',1913,'Scultura','Legno','58 x 36 x 23 cm','Pablo Picasso','Spagna2',11,13),(51,'La capra',1950,'Scultura','Bronzo','120 x 71 x 144 cm','Pablo Picasso','Spagna2',11,13),(52,'Donna con le braccia tese',1961,'Scultura','Lamiera','183 x 177.5 x 72.5 cm','Pablo Picasso','Spagna2',11,13),(53,'Dedalo e Icaro',1779,'Scultura','Marmo','220 cm','Antonio Canova','Italia4',12,8),(54,'Teseo sul Minotauro',1783,'Scultura','Marmo','145,4×158,7×91,4 cm','Antonio Canova','Italia4',12,8),(55,'Amore e Psiche',1793,'Scultura','Marmo','155 cm','Antonio Canova','Italia4',12,8),(56,'Adone e Venere',1794,'Scultura','Marmo','180×80×60 cm','Antonio Canova','Italia4',12,8),(57,'Tre Grazie',1816,'Scultura','Marmo','182 cm','Antonio Canova','Italia4',12,8),(58,'Ragazza con l\'orecchino di perla ',1666,'Pittura','Olio su tela','44,5×39 cm','Jan Vermeer','Olanda1',13,9),(59,'Lattaia',1660,'Pittura','Olio su tela','45,4×40,6 cm','Jan Vermeer','Olanda1',13,9),(60,'Astronomo',1668,'Pittura','Olio su tela','50×45 cm','Jan Vermeer','Olanda1',13,9),(61,'Fantesca che porge una lettera alla signora',1667,'Pittura','Olio su tela','90,2×78,7 cm','Jan Vermeer','Olanda1',13,9),(62,'Stradina di Delft',1658,'Pittura','Olio su tela','53,5×43,5 cm','Jan Vermeer','Olanda1',13,9),(63,'Autoritratto con l\'orecchio bendato',1889,'Pittura','Olio su tela','60x49 cm','Vincent van Gogh','Olanda1',14,9),(64,'Il ponte di Langlois',1888,'Pittura','Olio su tela','59x74 cm','Vincent van Gogh','Olanda1',14,9),(65,'I girasoli',1888,'Pittura','Olio su tela','91x72 cm','Vincent van Gogh','Olanda2',14,10),(66,'La camera di Vincent ad Arles',1888,'Pittura','Olio su tela','72x90 cm','Vincent van Gogh','Olanda2',14,10),(67,'La sedia di Vincent',1888,'Pittura','Olio su tela','93x73,5 cm','Vincent van Gogh','Olanda2',14,10),(68,'Il cortile dell\'ospedale di Arles',1889,'Pittura','Olio su tela','73 x 92,0 cm','Vincent van Gogh','Olanda2',14,10),(69,'Davanti al manicomio di Saint-Rémy',1889,'Pittura','Olio su tela','58x45 cm','Vincent van Gogh','Olanda2',14,10),(70,'Lillà',1889,'Pittura','Olio su tela','73x92 cm','Vincent van Gogh','Olanda2',14,10),(71,'Iris',1889,'Pittura','Olio su tela','71x93 cm','Vincent van Gogh','Olanda3',14,11),(72,'Notte stellata',1889,'Pittura','Olio su tela','73x92 cm','Vincent van Gogh','Olanda3',14,11),(73,'Ritratto del postino Joseph Roulin',1886,'Pittura','Olio su tela','65x54 cm','Vincent van Gogh','Olanda3',14,11),(74,'Ritratto del dottor Gachet',1890,'Pittura','Olio su tela','68x57 cm','Vincent van Gogh','Olanda3',14,11);

DROP TABLE IF EXISTS `tblSale`;

CREATE TABLE `tblSale` (
  `idSala` INT NOT NULL PRIMARY KEY,
  `sala` VARCHAR(12) NOT NULL
) ENGINE=InnoDB CHARACTER SET=latin1;

INSERT INTO `tblSale` VALUES (1,'Francia1'),(2,'Francia2'),(3,'Francia3'),(4,'Inghilterra1'),(5,'Italia1'),(6,'Italia2'),(7,'Italia3'),(8,'Italia4'),(9,'Olanda1'),(10,'Olanda2'),(11,'Olanda3'),(12,'Spagna1'),(13,'Spagna2');
