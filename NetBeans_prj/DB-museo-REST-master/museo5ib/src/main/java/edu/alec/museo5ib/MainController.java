package edu.alec.museo5ib;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Alessandro Cazziolato
 */

@RestController
public class MainController {

	@RequestMapping("/")
	public String index() {
		return "Hello da museo5ib";
	}

	@GetMapping("/ita")
	public String indexITA() {
		return "CIAO da museo5ib";
	}

	@RequestMapping("/v2/")
	public String index2() {
		return "Hello da museo5ib";
	}

	@GetMapping("/v2/ita")
	public String indexITA2() {
		return "CIAO da museo5ib";
	}

}
