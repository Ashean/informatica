package edu.alec.museo5ib.controller;

import edu.alec.museo5ib.entity.CorrenteArtistica;
import edu.alec.museo5ib.repository.CorrenteArtisticaRepo;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Alessandro Cazziolato
 */
@RestController
@RequestMapping("/correntiArtistiche")
public class CorrenteArtisticaController {

	@Autowired
	CorrenteArtisticaRepo correnteArtisticaRepo;

	@GetMapping()
	public List<CorrenteArtistica> getAllCorrentiArtistiche() {
		return correnteArtisticaRepo.findAll();
	}
}
