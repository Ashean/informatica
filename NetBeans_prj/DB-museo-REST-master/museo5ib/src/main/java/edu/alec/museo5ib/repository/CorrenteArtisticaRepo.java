package edu.alec.museo5ib.repository;

import edu.alec.museo5ib.entity.CorrenteArtistica;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Alessandro Cazziolato
 */
@Repository
public interface CorrenteArtisticaRepo extends CrudRepository<CorrenteArtistica, Long> {
	@Override
	public List<CorrenteArtistica> findAll();
}
