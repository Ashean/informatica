package edu.alec.museo5ib.controller;

import edu.alec.museo5ib.entity.Nazione;
import edu.alec.museo5ib.repository.NazioneRepo;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Alessandro Cazziolato
 */
@RestController
@RequestMapping("/nazioni")
public class NazioneController {

	@Autowired
	NazioneRepo NazioneRepo;

	@GetMapping()
	public List<Nazione> getAllNazioni() {
		return NazioneRepo.findAll();
	}
}
