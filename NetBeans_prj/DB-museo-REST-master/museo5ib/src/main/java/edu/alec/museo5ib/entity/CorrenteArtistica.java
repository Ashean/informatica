package edu.alec.museo5ib.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author Alessandro Cazziolato
 */
@Entity
@Table(name = "tblCorrenteArtistica")
@Data public class CorrenteArtistica {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private long idCorrenteArtistica;
	@Column(nullable=false)
	private String correnteArtistica;

}
