package edu.alec.museo5ib.repository;

import edu.alec.museo5ib.entity.Nazione;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Alessandro Cazziolato
 */
@Repository
public interface NazioneRepo extends CrudRepository<Nazione, Long> {
	@Override
	public List<Nazione> findAll();
}
