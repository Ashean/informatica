package edu.alec.museo5ib.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

/**
 *
 * @author Alessandro Cazziolato
 */
@Entity
@Table(name = "tblNazione")
@Data public class Nazione {

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private long idNazione;
	@Column(nullable=false)
	private String nazione;

}
