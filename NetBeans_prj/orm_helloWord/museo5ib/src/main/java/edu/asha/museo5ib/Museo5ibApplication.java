package edu.asha.museo5ib;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Museo5ibApplication {

	public static void main(String[] args) {
		SpringApplication.run(Museo5ibApplication.class, args);
	}

}
