/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.fogliodicarta.museo5ib.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
/**
 *
 * @author ashea
 */ 
@Entity
@Table(name="tblCorrenteArtistica")
@Data public class CorrenteArtistica {
    @Id 
    @GeneratedValue(strategy=GenerationType.SEQUENCE)
    long idCorrenteArtistica;
    @Column(nullable=false)
    String correnteArtistica; 

    /*public CorrenteArtistica() {
    }

    public CorrenteArtistica(long idCorrenteArtistica, String correnteArtistica) {
        this.idCorrenteArtistica = idCorrenteArtistica;
        this.correnteArtistica = correnteArtistica;
    }

    public long getIdCorrenteArtistica() {
        return idCorrenteArtistica;
    }

    public void setIdCorrenteArtistica(long idCorrenteArtistica) {
        this.idCorrenteArtistica = idCorrenteArtistica;
    }

    public String getCorrenteArtistica() {
        return correnteArtistica;
    }

    public void setCorrenteArtistica(String correnteArtistica) {
        this.correnteArtistica = correnteArtistica;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 47 * hash + (int) (this.idCorrenteArtistica ^ (this.idCorrenteArtistica >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CorrenteArtistica other = (CorrenteArtistica) obj;
        if (this.idCorrenteArtistica != other.idCorrenteArtistica) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CorrenteArtistica{" + "idCorrenteArtistica=" + idCorrenteArtistica + ", correnteArtistica=" + correnteArtistica + '}';
    }
    */
    
}
