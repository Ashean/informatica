/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.fogliodicarta.museo5ib;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Matteo
 */
@RestController
public class MainController {
    @RequestMapping("/")
    public String index(){
        return"Hello";
    }
}
